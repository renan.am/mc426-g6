FROM ubuntu:18.04


RUN apt-get update
RUN apt-get install -y \
    git \
    python3 \
    python3-pip \
    python3-requests \
    python3-pyqt5 \
    python3-pyqtgraph

RUN pip3 install pyinstaller

#RUN pyinstaller -n "stock" --add-data StockRalpme/gui/main_window.ui:StockRalpme/gui --add-data StockRalpme/data:StockRalpme/data --add-data StockRalpme/api_handler/api_info.ini:StockRalpme/api_handler StockRalpme/__main__.py
#RUN pip3 install PyQt5 Requests

#RUN apt-get install xorg openbox -y 

#CMD [ "python3", "-m", "StockRalpme"] 

CMD git clone https://gitlab.com/renan.am/mc426-g6.git cod && cd cod && python3 -m unittest discover && cd .. && pyinstaller -n stock --add-data cod/StockRalpme/gui/main_window.ui:StockRalpme/gui --add-data cod/StockRalpme/data:StockRalpme/data --add-data cod/StockRalpme/api_handler/api_info.ini:StockRalpme/api_handler cod/StockRalpme/__main__.py && cd /dist/stock && ./stock