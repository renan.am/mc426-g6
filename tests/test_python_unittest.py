import unittest
from StockRalpme.math_analyser import math_analyser as ma
from collections import namedtuple

# Testes de Valores Limite.
# O objetivo é verificar se as funções retornam resultados plausíveis 
# de acordo com as características dos valores das ações do mercado financeiro.

class math_analysis_test_cases(unittest.TestCase):

    def testMeanMethod(self):

        # Initialize constant values
        test_values = namedtuple("stock_values", "date open high low close volume")
        test = list()
        for i in range(100):
            date ="XX-XX-XXXX"
            open = 100
            high = 100
            low = 0
            close = 50
            volume = 1000
            value = test_values(date, open, high, low, close, volume)
            test.append(value)
        test_result = ma.math_analysis.mean(test,10)
        #
        eps = 1e-14
        for i in test_result:
            assert abs(i-50) < eps, "mean() not calculating values correctly"

    def testLimitMean(self):
        # Initialize constant values
        test_values = namedtuple("stock_values", "date open high low close volume")
        test = list()
        for i in range(100):
            date ="XX-XX-XXXX"
            open = 100
            high = 100
            low = 0
            close = 50
            volume = 1000
            value = test_values(date, open, high, low, close, volume)
            test.append(value)
        test_result = ma.math_analysis.mean(test,10)
        eps = 1e-14
        for i in test_result:
            assert i < high and i > low, "Mean Result not valid!"


    def testStdDevMethod(self):
        # Initialize constant values
        test_values = namedtuple("stock_values", "date open high low close volume")
        test = list()
        for i in range(100):
            date ="XX-XX-XXXX"
            open = 100
            high = 100
            low = 0
            close = 50
            volume = 1000
            value = test_values(date, open, high, low, close, volume)
            test.append(value)
        test_result = ma.math_analysis.std_deviation(test,10)
        eps = 1e-14
        for i in test_result:
            assert abs(i-0) < eps, "mean() not calculating values correctly"

    def testLimitstdDev(self):
        # Initialize constant values
        test_values = namedtuple("stock_values", "date open high low close volume")
        test = list()
        for i in range(100):
            date ="XX-XX-XXXX"
            open = 100
            high = 100
            low = 0
            close = 50
            volume = 1000
            value = test_values(date, open, high, low, close, volume)
            test.append(value)
        test_result = ma.math_analysis.std_deviation(test,10)
        eps = 1e-14
        for i in test_result:
            assert i >= 0, "Mean Result not valid!"

    def testLimitRsi(self):
        # Initialize constant values
        test_values = namedtuple("stock_values", "date open high low close volume")
        test = list()
        for i in range(100):
            date ="XX-XX-XXXX"
            open = 100
            high = 100
            low = 0
            close = 50
            volume = 1000
            value = test_values(date, open, high, low, close, volume)
            test.append(value)
        test_result = ma.math_analysis.rsi_sma(test)
        #
        eps = 1e-14
        assert test_result < 100 or test_result > 0, "Mean Result not valid!"




if __name__ == "__main__":
    unittest.main() # run all tests