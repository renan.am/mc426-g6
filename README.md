# MC426 - Grupo 6
# Análise de ações da bolsa de valores

Armazenar o histórico do preço de ações em um banco de dados, que permita um usuário aplicar funções de análise sobre essas informações.

![foto1](https://i.imgur.com/787LyIk.png)

![foto2](https://i.imgur.com/9ModnSD.png)

### O documento Particionamento das Atividades descreve a contribuição e as avaliações de cada membro do grupo. 

[ATV-A3] Documentos de atividade 3 (A3), se encontram na pasta /DOC, com prefixo "A3"

[ATV-A4] Documentos de atividade 4 (A4), se encontram na pasta /DOC, com prefixo "A4" 

[ATV-A5] Documentos de atividade 5 (A5), se encontram na pasta /DOC, com prefixo "A5"

[ATV-A6] Criamos as branches best_stock_by_parameter e day_average para adicionar as respectivas features (funções de mesmo nome). 
Depois, terminamos a implementação, testamos, e fizemos o merge de volta para o branch dev.

[ATV-A7] Dockerfile criado na pasta raiz, vale ressaltar que para a interface gráfica funcionar atavés do docker deve ser feita a conexão com o gerenciador de janela apropriado, nossos testes foram executados no windows, e funcionou perfeitamente
         Executar o docker para compilar o programa e executar testes, para executar sem compilar, rodar o comando "Python -m StockRalpme" com as bibliotecas em "requiriments.txt" propiamente instaladas

### O módulo de testes se encontra na pasta /tests

