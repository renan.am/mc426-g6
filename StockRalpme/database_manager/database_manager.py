import sqlite3
from collections import namedtuple
from pathlib import Path
from ..api_handler import api_alpha_vantage as api_av

class database_manager():

    # constructor
    def __init__(self, absolute_database_path, api_obj):

        # Check if the file exists, if doesn't, create one. 
        db_file = Path(absolute_database_path)
        if not db_file.is_file():
            db_file=open(absolute_database_path, 'w+')

        # Connecting to SQLite database.
        self.connection=sqlite3.connect(absolute_database_path)
        self.cursor=self.connection.cursor()
        self.api_obj = api_obj


    # function: request_api_data
    # Populates the database with a information about company from the 
    # api.
    def request_api_data(self, company_code):
        #Create the table in the db if not exists yet
        create_query = "create table if not exists " + company_code + "("
        create_query += "date TEXT PRIMARY_KEY,"
        create_query += "open FLOAT,"
        create_query += "highest FLOAT,"
        create_query += "lowest FLOAT,"
        create_query += "close FLOAT,"
        create_query += "volume FLOAT"
        create_query += ")"
        self.cursor.execute(create_query)

        # Get requested data from api
        data = self.api_obj.get_data("TIME_SERIES_DAILY", company_code)
        
        # Insert data in the new table created
        granularity = 'Time Series (Daily)'

        for x in data[granularity]:
            insert_query = "insert into " + company_code
            insert_query += " (date, open, highest, lowest, close, volume)"
            insert_query += "values ( '"
            insert_query +=  x + "',"
            insert_query +=  data[granularity][x]["1. open"] + ","
            insert_query +=  data[granularity][x]["2. high"] + ","
            insert_query +=  data[granularity][x]["3. low"] + ","
            insert_query +=  data[granularity][x]["4. close"] + ","
            insert_query +=  data[granularity][x]["5. volume"] + ")"
            self.cursor.execute(insert_query)
        self.connection.commit()

    # function: get_date
    # Retrive some information about a company from database, if it's not present
    # try to populate the faulty information using api.
    def get_data(self, company_code, date_begin, date_end):

        # TODO: Check validity of date.

        # Check if company already exists on internal database,
        self.cursor.execute("SELECT name FROM sqlite_master WHERE type ='table' AND name NOT LIKE 'sqlite_%';")

        company_is_present = False
        for row in self.cursor:
            if row[0] == company_code:
                company_is_present = True
                break

        # If not, make a request from API and populate db. 
        if company_is_present == False:
            self.request_api_data(company_code)
        
        stock_values = namedtuple("stock_values", "date open high low close volume")
        query = "select * from " + company_code + " where (date between '" + date_begin + "' and '" + date_end + "')"
        self.cursor.execute(query)

        result = list()
        for row in self.cursor:
            query_answer = stock_values(row[0], row[1], row[2], row[3], row[4], row[5])
            result.append(query_answer)
        return result


    def __del__(self):
        self.connection.close()