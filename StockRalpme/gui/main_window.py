import sys
from PyQt5 import QtWidgets, uic, QtGui, QtCore
import pyqtgraph as pg


class Ui(QtWidgets.QMainWindow):
    def __init__(self, state):
        super(Ui, self).__init__()
        self.state = state

        uic.loadUi('StockRalpme/gui/main_window.ui', self)

        self.state.stockSelected = "MSFT"

        self.stock_list = self.findChild(QtWidgets.QListView, 'stock_list')
        self.state.stock_list_items = QtGui.QStandardItemModel()
        self.stock_list.setModel(self.state.stock_list_items)

        self.init_stock_box()

        self.stock_list.clicked[QtCore.QModelIndex].connect(self.stock_list_item_selected)

        #Labels colunas
        self.selected_stock_name = self.findChild(QtWidgets.QLabel, 'selected_stock_name') 
        self.selected_stock_high = self.findChild(QtWidgets.QLabel, 'selected_stock_high')
        self.selected_stock_low = self.findChild(QtWidgets.QLabel, 'selected_stock_low')
        self.selected_stock_close = self.findChild(QtWidgets.QLabel, 'selected_stock_close')
        self.selected_stock_open = self.findChild(QtWidgets.QLabel, 'selected_stock_open')
        self.selected_stock_volume = self.findChild(QtWidgets.QLabel, 'selected_stock_volume')

        #data
        self.state.data1 = self.findChild(QtWidgets.QDateEdit, 'dataini')
        self.state.data2 = self.findChild(QtWidgets.QDateEdit, 'datafim')
        
        
        


        self.bt_new_window = self.findChild(QtWidgets.QPushButton, 'graph')
        self.bt_new_window.clicked.connect(self.new_window)

        #self.addItem()

        self.show()

    def addItem(self):
        new_item = QtGui.QStandardItem
        self.state.stock_list_items.appendRow(new_item("AAPL"))
        self.state.stock_list_items.appendRow(new_item("MSFT"))

    
    def init_stock_box(self):
        self.stock_box = self.findChild(QtWidgets.QComboBox, 'stock_box')
        self.stock_box.addItems(self.state.get_all_stocks())
        
        self.bt_stock_box = self.findChild(QtWidgets.QPushButton, 'bt_stock_box')
        self.bt_stock_box.clicked.connect(self.add_stock_from_box)

    def add_stock_from_box(self):
        new_stock = self.stock_box.currentText()
        if new_stock == "":
            return
        else:
            new_item = QtGui.QStandardItem
            self.state.stock_list_items.appendRow(new_item(new_stock))

    def stock_list_item_selected(self, index):
        stock = self.state.stock_list_items.itemFromIndex(index)
        self.state.stockSelected = stock.text()
    
    def new_window(self):
        self.state.dataIni = str(self.state.data1.date().toPyDate())
        self.state.dataFim = str(self.state.data2.date().toPyDate())
     
        
        info = self.state.get_info(self.state.stockSelected, self.state.dataIni, self.state.dataFim)
        self.state.Y = self.state.get_mean(info)


        self.window = GRAPH(self.state)
        self.window.show()
        



class GRAPH(QtWidgets.QMainWindow):
    def __init__(self, state):
        super(GRAPH, self).__init__()
        self.state = state
        
        uic.loadUi('StockRalpme/gui/graph.ui', self)

        self.graphWidget = pg.PlotWidget()
        self.setCentralWidget(self.graphWidget)

        X = []
        Y = []

        temp = self.state.data1.date()

        for i in self.state.Y:
            Y.append(i)
            aux = str(temp.toPyDate())
            X.append(int(aux[-2:]))
            temp = temp.addDays(1)

        # plot data: x, y values
        self.graphWidget.plot(X, Y)



def start(state):
    app = QtWidgets.QApplication(sys.argv)
    window = Ui(state)
    app.exec_()