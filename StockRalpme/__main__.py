from StockRalpme.gui import main_window
from StockRalpme.api_handler import api_alpha_vantage as api_av
from StockRalpme.database_manager import database_manager 
from StockRalpme.state_manager import state_manager as sm

if __name__ == '__main__':
    #Inicializa uma instancia da api, atraves dela se acessa os dados do site
    ap = api_av.api_alpha()
    #Inicializa uma instancia do banco de dados, envia a localização padrão do arquivo (não precisa existir o arquivo)
    #Envia também a instancia da api para permitir o acesso de suas funções pela classe database_manager
    db=database_manager.database_manager("StockRalpme/data/stock_database.db", ap)
    #Inicializa uma instancia de state, classe responsavel por fazer a comunicação entre o backend(db) e frontend (GUI)
    #também é responsável por armazenar os dados da interface gráfica
    state = sm.gui_state(db)
    
    # inicizaliza o GUI, enviando a instancia de state que será usada para se comunicar com o backend
    main_window.start(state)


