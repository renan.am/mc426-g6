import math
class math_analysis():

    # Name: Mean
    # Description: Calculates the mean of a time serie given an
    # discretization. The values used was the mean of the day,
    # based in (high+low)/2.
    # Example:
    # time_series = (10, 10, 9, 9, 8, 8) Discretization = 3
    # Result = (10, 9, 8)
    # Arguments:
    # time_series - tuple with values of date, open, high, low, close in order
    # discretization - integer representing the number of values to consider in 
    # each mean.
    # return list with the values of mean
    @staticmethod
    def mean(time_series, discretization):

        # Check if is possible partitionate len(time_series) in 
        # Discretization values.
        if len(time_series) < discretization:
            raise Exception('Not possible to discretize the given time serie')

        result = list()
        result = [0]*discretization
        window = math.ceil(len(time_series)/discretization)
        for i in range(discretization):
            accumulator = 0
            counter = 0
            for j in range(i*window, min( (i+1)*window , len(time_series)) ):
                accumulator+=(time_series[j][2]+time_series[j][3])/2
                counter += 1
            result[i] = accumulator/counter
        
        return result

    # Name: std_deviation
    # Description: Calculates the std_deviation of a time serie given an
    # discretization. The values used was the std_deviation of the day,
    # based in (high+low)/2.
    # Example:
    # time_series = (10, 10, 9, 9, 8, 8) Discretization = 3
    # Result = (0,0,0)
    # Arguments:
    # time_series - tuple with values of date, open, high, low, close in order
    # discretization - integer representing the number of values to consider in 
    # each standard deviation.
    # return list with the values of std_deviation
    @staticmethod
    def std_deviation(time_series, discretization):

        # Check if is possible partitionate len(time_series) in 
        # Discretization values.
        if len(time_series) < discretization:
            raise Exception('Not possible to discretize the given time serie')

        result = list()
        result = [0]*discretization
        window = math.ceil(len(time_series)/discretization)
        average = math_analysis.mean(time_series,discretization)
        for i in range(discretization):
            accumulator = 0
            counter = 0
            for j in range(i*window, min( (i+1)*window , len(time_series)) ):
                accumulator+=math.pow(average[i] - (time_series[j][2]+time_series[j][3])/2,2)
                counter += 1
            result[i] = math.sqrt(accumulator/counter)
        
        return result


    # Name: rsi_sma
    # Description: Calculates the Relative Strength Index(RSI) in a given period
    # using simple moving average.
    # Arguments:
    # time_series - tuple with values of date, open, high, low, close in order
    # must be ordered by date.
    # return rsi value
    @staticmethod
    def rsi_sma(time_series):
        # Check if there is at least one value in time serie
        if len(time_series) < 2:
            raise Exception('rsi needs at least 2 values in time series')
        down = 0
        up = 0
        for j in range(1, len(time_series)):
            change = time_series[j][4] - time_series[j-1][4]
            if change < 0:
                up += -change
            elif change > 0:
                down += change
        avgu = up/len(time_series)
        avgd = down/len(time_series)
        if avgd == 0:
            rsi = 100
        else:
            rs = avgu/avgd
            rsi = 100 - 100/(1+rs)
        
        return rsi
                


        
