import random
from ..math_analyser import math_analyser as ma

class gui_state:
    def __init__(self, db):
        self.db = db
        self.old_data = []
        self.data = [[9, 8.1, 5.6, 6.7, 8.9, 6.7],
                    [5.4, 11.6, 9.3, 17.8, 9.5, 22.3],
                    [10, 5,4, 2,8, 6.7, 90.5, 19.0],
                    [18.3, 56.3, 65.5, 18.2, 23.4, 82.5],
                    [50, 750.9, 0.111, 6, 88.98, 100]]
        self.stocks = [
            ("MSFT", [9, 8.1, 5.6, 6.7, 8.9, 6.7]),
            ("AAPL", [18.3, 56.3, 65.5, 18.2, 23.4, 82.5]),
            ("AMZN", [10, 5,4, 2,8, 6.7, 90.5, 19.0]),
            ("GOGL", [50, 750.9, 0.111, 6, 88.98, 100])]
        self.idx = {"date":0, "open":1, "high":2, "low":3, "close":4, "volume":5}

    def test(self):
        try:
            data = self.db.get_data("MSFT", "2019-06-20", "2019-07-21")
        except Exception as e:
            print ("Erro ao tentar adquirir dados")
            print (e)
            data = []
        if data == []:
            data = [[9, 8.1, 5.6, 6.7, 8.9, 6.7],
                    [5.4, 11.6, 9.3, 17.8, 9.5, 22.3],
                    [10, 5,4, 2,8, 6.7, 90.5, 19.0],
                    [18.3, 56.3, 65.5, 18.2, 23.4, 82.5],
                    [50, 750.9, 0.111, 6, 88.98, 100]]
        self.data = data
        return ("2019-10-29", data[0][1], data[0][2], data[0][3], data[0][4], data[0][5])

    def get_info(self, stock, dataIni, dataFim):
        try:
            data = self.db.get_data(stock, dataIni, dataFim)
        except Exception as e:
            print ("Erro ao tentar adquirir dados")
            print (e)
            data = []
        if data == []:
            data = [[9, 8.1, 5.6, 6.7, 8.9, 6.7],
                    [5.4, 11.6, 9.3, 17.8, 9.5, 22.3],
                    [10, 5,4, 2,8, 6.7, 90.5, 19.0],
                    [18.3, 56.3, 65.5, 18.2, 23.4, 82.5],
                    [50, 750.9, 0.111, 6, 88.98, 100]]
        self.data = data
        return data

    def get_mean(self, info):
        try:
            data = ma.math_analysis.mean(info, len(info))
        except Exception as e:
            print ("Erro ao tentar adquirir dados")
            print (e)
            data = [x[1] for x in info]
        return data

    def day_average(self):
        average = []
        for x in self.data:
            # x[1] = open
            # x[4] = close
            aux = (x[1]+x[4])/2
            average.append(aux)

        return average
            
    def best_stock_by_parameter(self, paramater="volume"):
        par = self.idx[paramater]
        val = 0
        name = ""
        for x in self.stocks:
            if val < x[1][par]:
                name, val = x[0], x[1][par]
        
        return name

    def get_all_stocks(self):
        return ["AAPL", "AMZN", "GOGL", "MSFT" ]
