# Api para adquirir dados da alpha vantage
import json
import requests
import datetime
import configparser

# checa se existe um arquivo .ini com um chave de api para ser usado, se não existir
# ele usa a chave default "demo" fornecida pelo site, porém limitada
cfg = configparser.ConfigParser()

try:
    cfg.read('StockRalpme/api_handler/api_info.ini')
    default_key = cfg.get('ALPHA_VANTAGE', 'api_key')
except:
    default_key = "demo"

class api_alpha():

    def __init__(self, apikey=default_key):
        self.url = "https://www.alphavantage.co/query?function={function}&symbol={symbol}&apikey={apikey}"
        self.number_of_uses = 0
        self.time_of_creation = self.get_current_date()
        self.api_key = apikey

    @staticmethod
    def get_current_date():
        now = datetime.datetime.now()
        year = '{:02d}'.format(now.year)
        month = '{:02d}'.format(now.month)
        day = '{:02d}'.format(now.day)
        hour = '{:02d}'.format(now.hour)
        minute = '{:02d}'.format(now.minute)
        day_month_year = '{}-{}-{}'.format(year, month, day)
        return day_month_year
    # pega informações do site, variaveis sao iniciadas com dados default
    def get_data(self, func="TIME_SERIES_DAILY", symb="MSFT"):
        response = requests.get(self.url.format(function=func, apikey=self.api_key, symbol=symb))
        self.number_of_uses += 1
        return json.loads(response.text)